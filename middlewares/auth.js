const jwt = require('jsonwebtoken')
const config = require('config')
const MSGS = require('../messages')
const JWTSECRET = process.env.JWTSECRET || config.get('JWTSECRET')


module.exports = (req, res, next) => {
    const token = req.header('x-auth-token')

    if(!token){
        return res.status(401).send({ "error" : MSGS.NO_TOKEN })
    }

    try{
        jwt.verify(token, JWTSECRET, (error, decoded) => {
            if(error){
                return res.status(401).send({ "error": MSGS.INVALID_TOKEN})
            } else {
                req.user = decoded.user
                next()
            }
        })

    } catch (error){
        console.error(error.message)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }

}