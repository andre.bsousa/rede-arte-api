const config = require('config')
const mongoose = require('mongoose')

const connectDB = async (db= process.env.MONGO_URI || config.get('MONGO_URI')) => {
    try{
        await mongoose.connect(db, {
			useNewUrlParser: true,
			useCreateIndex: true,
			useFindAndModify: false,
			useUnifiedTopology: true
        })
        console.log('MongoDB Connected...')
    } catch (error) {
        console.error(error.message);
		// Exit process with failure
		process.exit(1);
    }

}

module.exports = connectDB