const MSGS = {
    'GENERIC_ERROR': 'Aconteceu algo inexperado',
    'INVALID_TOKEN': 'Token invalido',
    'NO_TOKEN': 'Sem token, autorização negada',
    'PROFILE404': 'Perfil não encontrado',
    'USER401': 'Credencial invalida',
    'USER404': 'Usuario não encontrado',
    'USER409': 'Usuario ja existe',

}

module.exports = MSGS